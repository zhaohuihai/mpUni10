

# create list of object files
obj_files = $(subst .cpp,.o,$(wildcard *.cpp))
objects := $(foreach filename,$(obj_files),$(obj_dir)/$(filename) )

# for each object file foo.o, create a dependency file foo.d
dep_files = $(subst .cpp,.d,$(wildcard *.cpp))
dependencies := $(foreach filename,$(dep_files),$(dep_dir)/$(filename) )

# when make is run with no arguments, create the executable
.PHONY : default
default : $(objects)

# for each object file foo.o, create a dependency file foo.d in the dep/ directory
$(dependencies) : $(dep_dir)/%.d : %.cpp
	@echo "updating dependencies for $<";
	@$(CC) $(CC_FLAGS) $(GLOBAL_INC) -MM $< | sed -e "s^$*\.o^$(obj_dir)/$*.o $(dep_dir)/$*.d^" > $@;
	
# read in the rules that list the dependencies for each object and dependency file
sinclude $(dependencies)

# compile each object file
$(objects) : $(obj_dir)/%.o : %.cpp
	$(CC) -c $(CC_FLAGS) $(GLOBAL_INC) $(OPTION) $(REPORT) $< -o $@;

# show
.PHONY : show
show:
	@echo "object dir"
	@echo $(obj_dir)
	@echo "object files"
	@echo $(objects)
	@echo "dependency files"
	@echo $(dependencies)
	@echo "global include"
	@echo $(GLOBAL_INC)

# rule to clean up object and dependency files
.PHONY : clean
clean : 
	rm -f $(obj_dir)/*.o;
	rm -f $(dep_dir)/*.d;