/*
 * fermionic_projected_entangled_pair_state.h
 *
 *  Created on: 2015年10月21日
 *      Author: zhaohuihai
 */

#ifndef SRC_FERMIONIC_PROJECTED_ENTANGLED_PAIR_STATE_H_
#define SRC_FERMIONIC_PROJECTED_ENTANGLED_PAIR_STATE_H_

#include "uni10/uni10.hpp"

class FermionicProjectedEntangledPairState
{
private:
	int _length_x ;
	int _length_y ;
	int _nsites ;              // number of sites in the lattice
	int _siteDim ;             // dim of physical index
	int _bondDim ;             // dim of bond index
	int _dimEven ;             // dim for even sector
	int _dimOdd ;              // dim for odd sector

	int _chi ;                 // truncation dimension
	double _tol ;              // tolerance in canonical condition
	int _maxIt ;               // max number of iteration

	int _numel ;               // number of total elements
	std::vector<bool> _ti ;    // whether a tensor is a translated image of another tensor
	std::vector<int> _tiSite ; // if a translated image, the site defining the image's parent
	// pointers to site tensors element [site][phys]
	std::vector< std::vector<double*> >  _siteElem ;
	std::vector< int > _eleNum ; // electron configuration
	std::vector< int > _new_eleNum ; // electron configuration
	double _weight ;
	double _new_weight ;

	std::vector< std::vector< uni10::UniTensor > > _rowMPOs ; // mpo from all sites except one row
	std::vector< std::vector< uni10::UniTensor > > _colMPOs ; // mpo from all sites except one column

	void set_ti() ; // set translational invariant sites
	int getSiteElemNum(const int dimEven, const int dimOdd, const int physInd) ;
	uni10::UniTensor getSiteTensor(const int iLattice, const int physInd) ;
	int getPhysInd(const int* eleNum, const int i) ;
	int getPhysInd(const std::vector<int>& eleNum, const int i) ;

public:
	// constructor
	FermionicProjectedEntangledPairState() ; // default constructor
	FermionicProjectedEntangledPairState(const int length_x, const int length_y,
																			 const int siteDim,  const int bondDim, const int dimEven,
																			 double* elem, const int chi, const double tol, const int maxIt) ;
	// ---- member access
	int length_x() const { return _length_x ; }
	int length_y() const { return _length_y ; }
	int nsites() const { return _nsites ; }
	int siteDim() const { return _siteDim ; }
	int bondDim() const { return _bondDim ; }
	int dimEven() const { return _dimEven ; }
	int dimOdd() const { return _dimOdd ; }

	int getChi() const { return _chi ; }
	double tol() const { return _tol ; }
	int maxIt() const { return _maxIt ; }

	int numel() const { return _numel ; }
	std::vector<bool> ti() const { return _ti ; }
	std::vector<int> tiSite() const { return _tiSite ; }
	std::vector< std::vector<double*> > siteElem() const { return _siteElem ; }
	std::vector< int > eleNum() const { return _eleNum ; }
	std::vector< int > new_eleNum() const { return _new_eleNum ; }
	double weight() const { return _weight ; }
	double new_weight() const { return _new_weight ; }
	std::vector< std::vector<uni10::UniTensor> > rowMPOs() const { return _rowMPOs; }
	std::vector< std::vector<uni10::UniTensor> > colMPOs() const { return _colMPOs; }
	//----------------------------------------------------------------------
	void initMakeSample(const int *eleNum) ;
	void initPhysCal(const int *eleNum) ;
	void update() ;
	//----------------------------------------------------------------------
	double compute_weight(const int* eleNum) ;
	double compute_weight(const std::vector<int>& eleNum) ;
	// compute <x'|SBS> / <x|SBS>
	double computeRatio(const int ri, const int rj, const int s) ;
	double computeRatio(const int ri, const int rj, const int s,
											const int rk, const int rl, const int t) ;
	// compute log(<x'|PEPS> / <x|PEPS>)
	double computeLogRatio(const int ri, const int rj, const int s) ;
	double computeLogRatio(const int ri, const int rj, const int s,
												 const int rk, const int rl, const int t) ;

	// derivatives with respect to every variational parameter
	void derivative(double *der, const int* eleNum) ;
	double* getSitePtr(double *der, int i, int physInd) ;
	void siteDerivative(const int* eleNum, int i, double* siteDerPtr) ;

	void updateMPO(std::vector<uni10::UniTensor>& upMPO, std::vector<uni10::UniTensor>& downMPO,
								 std::vector<uni10::UniTensor>& udMPO) ;
	void canonicalize(std::vector<uni10::UniTensor>& upMPO, std::vector<uni10::UniTensor>& downMPO,
			 	 	 	 	 	 	  std::vector<uni10::UniTensor>& udMPO) ;
	void getGauge(uni10::UniTensor& T1, uni10::UniTensor& T2,
			          uni10::UniTensor& T3, uni10::UniTensor& T4,
			          uni10::UniTensor& PRA, uni10::UniTensor& PLB, uni10::UniTensor& S,
								double& truncErr) ;
	uni10::UniTensor contractWithLeftMirror(uni10::UniTensor& T1, uni10::UniTensor& T2) ;
	uni10::UniTensor contractWithRightMirror(uni10::UniTensor& T3, uni10::UniTensor& T4) ;
	void decomposeM(uni10::UniTensor& M) ;
	void truncateSVD(uni10::UniTensor& U, uni10::UniTensor& S, uni10::UniTensor& V, double& truncErr) ;
	int getChi(uni10::UniTensor& S) ;
	uni10::UniTensor createProjector(uni10::UniTensor Mb, uni10::UniTensor V, uni10::UniTensor sqrtS) ;
	void updateA(uni10::UniTensor& PLA, uni10::UniTensor& T1, uni10::UniTensor& T2,
			         uni10::UniTensor& PRA, uni10::UniTensor& A) ;

	void updateColMPO(std::vector<uni10::UniTensor>& lMPO, std::vector<uni10::UniTensor>& rMPO,
			 	 	 	 	 	 	 	std::vector<uni10::UniTensor>& lrMPO) ;
	void canonicalizeCol(std::vector<uni10::UniTensor> lMPO, std::vector<uni10::UniTensor> rMPO,
	 	 	 	 							 std::vector<uni10::UniTensor>& lrMPO) ;
	void verticalContract(uni10::UniTensor& T1, uni10::UniTensor& T2, uni10::UniTensor& newT) ;
	void horizontalContract(uni10::UniTensor& T1, uni10::UniTensor& T2, uni10::UniTensor& newT) ;

	double contract2RowMPOs(std::vector<uni10::UniTensor>& upMPO, std::vector<uni10::UniTensor>& downMPO) ;
	double contract2ColMPOs(std::vector<uni10::UniTensor>& lMPO, std::vector<uni10::UniTensor>& rMPO) ;

	void verticalTrace(uni10::UniTensor& T1, uni10::UniTensor& T2, uni10::UniTensor& M) ;
	void horizontalTrace(uni10::UniTensor& T1, uni10::UniTensor& T2, uni10::UniTensor& M) ;

	void computeRowMPO(const int *eleNum, int j, std::vector<uni10::UniTensor>& rowMPO) ;
	void computeColMPO(const int *eleNum, int i, std::vector<uni10::UniTensor>& colMPO) ;
	//----------------------------------------------------------------------
	void rescale(double maxElem) ;
	void info() ;
	void disp() ;
};

int FPEPSsiteElemNum(const int siteDim,  const int bondDim, const int dimEven) ;

#endif /* SRC_FERMIONIC_PROJECTED_ENTANGLED_PAIR_STATE_H_ */
