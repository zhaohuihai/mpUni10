/*
 * main.cpp
 *
 *  Created on: 2015-10-20
 *      Author: zhaohuihai
 */

#include "uni10/uni10.hpp"
#include "fermionic_projected_entangled_pair_state.h"

using namespace std ;

void egU1()
{

	// Since it has U1 symmetry(total Sz conserved)
	// add U1 quantum number to the states of bonds.
	uni10::Qnum qfe(uni10::PRTF_EVEN);
	uni10::Qnum qfo(uni10::PRTF_ODD);
  std::vector<uni10::Qnum> qnums;
	qnums.push_back(qfe);
	qnums.push_back(qfo);
	qnums.push_back(qfo);
	qnums.push_back(qfe);
	// Create in-coming and out-going bonds
	uni10::Bond bd_in(uni10::BD_IN, qnums);
	qnums.clear() ;
	qnums.push_back(qfe);
	qnums.push_back(qfo) ;
//	bd_out.assign(uni10::BD_IN, qnums);
	uni10::Bond bd_out(uni10::BD_OUT, qnums);
	qnums.clear() ;
//	qnums.push_back(qfe) ;
//	qnums.push_back(qfe) ;
	qnums.push_back(qfo) ;
//	qnums.push_back(qfe) ;
	uni10::Bond bd_phys(uni10::BD_OUT, qnums);
	std::vector<uni10::Bond> bonds;
	bonds.push_back(bd_in);
	bonds.push_back(bd_in);
	bonds.push_back(bd_out);
	bonds.push_back(bd_phys);
	bonds.push_back(bd_out);
	bonds.push_back(bd_phys);

	int label_old[] = {1000,1001,2000,3000,2002,3004} ;
	// Create tensor from the bonds and name it "T".
	uni10::UniTensor T(bonds, label_old, "T");
	// Assigns random numbers
	T.randomize();
	std::cout<< T;
	uni10::UniTensor T1 = T.permute(4) ;
	cout << T1 ;
exit(0) ;
	// Check the quantum number of the blocks
//	std::cout<<"The number of the blocks = "<< T.blockNum()<<std::endl;
//	std::vector<uni10::Qnum> block_qnums = T.blockQnum();
//	for(int q = 0; q < block_qnums.size(); q++)
//		std::cout<< block_qnums[q]<<", ";
//	std::cout<<std::endl<<std::endl;

	// Write out tensor
//	T.save("egU1_H_U1");

	int label_new[] = {1000, 1001, 2000, 2002, 3000, 3004} ;
	T = T.permute(label_new ,2) ;
	cout << T ;

//	// Check the quantum number of the blocks
//	std::cout<<"The number of the blocks = "<< T.blockNum()<<std::endl;
//	block_qnums = T.blockQnum();
//	for(int q = 0; q < block_qnums.size(); q++)
//		std::cout<< block_qnums[q]<<", ";
//	std::cout<<std::endl<<std::endl;
}

void egBond()
{
	// Fermionic system
	std::cout<<"----- Fermionic -----\n";
	// fermionic parity even, U1 = 1, parity even.
//	uni10::Qnum f0_q10(uni10::PRTF_EVEN, 1, uni10::PRT_EVEN);
	uni10::Qnum f0_q10(uni10::PRTF_EVEN);

	// fermionic parity odd, U1 = 1, parity even.
	uni10::Qnum f1_q10(uni10::PRTF_ODD);

	std::cout<<"f0_q10: "<<f0_q10<<std::endl;
	std::cout<<"f1_q10: "<<f1_q10<<std::endl;
	std::cout<<"f0_q10: fermionic parity = " <<f0_q10.prtF()<<std::endl;
	std::cout<<"f1_q10: fermionic parity = " <<f1_q10.prtF()<<std::endl;
	std::cout<<"isFermioinc: "<<uni10::Qnum::isFermionic()<<std::endl;

	std::vector<uni10::Qnum> qnums ;
	qnums.push_back(f0_q10) ;
	qnums.push_back(f1_q10) ;
	qnums.push_back(f1_q10) ;

	// Constrcut Bond with Qnum array
	uni10::Bond bd(uni10::BD_IN, qnums);
	// Print out a Bond
	std::cout<<"Bond bd: \n"<<bd<<std::endl;
	std::cout<<"Bond type: "<<bd.type()<<"(IN)"<<", Bond dimension: "<<bd.dim()<<std::endl<<std::endl;

	qnums.clear() ;
	qnums.push_back(f0_q10) ;
	qnums.push_back(f1_q10) ;
	qnums.push_back(f1_q10) ;
	uni10::Bond bd2(uni10::BD_IN, qnums);
	std::cout<<"Bond bd2: \n"<<bd2<<std::endl;
	// bd.combine(bd2);
	std::cout<<"bd2.combine(bd): \n"<<bd2.combine(bd)<<std::endl;

	std::cout<<"Degeneracies of bd2 after combining bd: "<<std::endl;
	std::map<uni10::Qnum, int> degs = bd2.degeneracy();
	for(std::map<uni10::Qnum,int>::const_iterator it=degs.begin(); it!=degs.end(); ++it)
		std::cout<<it->first<<": "<<it->second<<std::endl;
	std::cout<<std::endl;
}

void test_fPEPS()
{
	const int length_x = 4;
	const int length_y = 4;
	const int siteDim = 4;
	const int bondDim = 2;
	const int dimEven = 1 ;
	double elem[1000];
	double siteDer[1000];
	for ( int i = 0; i < 1000; i ++ ) elem[i] = rand() ;
	const int chi = 100;
	const double tol = 1.0;
	const int maxIt = 1 ;
	FermionicProjectedEntangledPairState fpeps(length_x, length_y,
			siteDim, bondDim, dimEven, elem, chi, tol, maxIt) ;

	int eleNum[32] ;
	for ( int i = 0; i < 16; i ++ ) {
		eleNum[i] = 0 ;
	}
	for ( int i = 16; i < 32; i ++ ) {
		eleNum[i] = 1 ;
	}
	fpeps.rescale(1.0) ;
	fpeps.initPhysCal(eleNum) ;
	cout << fpeps.weight() << endl ;
//	cout << fpeps.numel() << endl  ;
//	double weight = fpeps.compute_weight(eleNum) ;
	fpeps.derivative(siteDer, eleNum) ;
}

void test_permute()
{
	uni10::Qnum qfe(uni10::PRTF_EVEN);
	uni10::Qnum qfo(uni10::PRTF_ODD);
  std::vector<uni10::Qnum> qnums;
	qnums.push_back(qfe);
//	qnums.push_back(qfo);
	// Create in-coming and out-going bonds
	uni10::Bond bd_e(uni10::BD_IN, qnums);
	qnums.clear() ;
	qnums.push_back(qfo) ;
	uni10::Bond bd_o(uni10::BD_IN, qnums);
	std::vector<uni10::Bond> bonds;
	bonds.push_back(bd_e);
	bonds.push_back(bd_o);
	bonds.push_back(bd_o);
	bonds.push_back(bd_e);

	int label[] = {1, 2, 3, 4} ;
	// Create tensor from the bonds and name it "T".
	uni10::UniTensor T(bonds, label, "T");
	// Assigns random numbers
	T.randomize();
	std::cout<< T;

	int Tlabel_new[] = {1, 3, 2, 4} ;
	T.permute(Tlabel_new, T.bondNum()) ;
	cout << T ;
}

void test_contractOrder()
{
	uni10::Qnum qfe(uni10::PRTF_EVEN);
	uni10::Qnum qfo(uni10::PRTF_ODD);
	vector<uni10::Qnum> qnums ;
	qnums.push_back(qfe);
	qnums.push_back(qfo);
	uni10::Bond bd_eo(uni10::BD_IN, qnums);
	qnums.clear() ;
	qnums.push_back(qfe);
	uni10::Bond bd_e(uni10::BD_IN, qnums);
	qnums.clear() ;
	qnums.push_back(qfo);
	uni10::Bond bd_o(uni10::BD_IN, qnums);

	vector<uni10::Bond> Abonds ;
	Abonds.push_back(bd_o) ; // 1
//	Abonds.push_back(bd_eo) ;
	Abonds.push_back(bd_eo) ;
	Abonds.push_back(bd_eo) ; // 2
	int Alabel [] = {1, 100, 2} ;
	uni10::UniTensor A(Abonds, Alabel, "A") ;
	A.randomize() ;
	A *= 10.0 ;

	vector<uni10::Bond> Bbonds ;
	Bbonds.push_back(bd_eo) ; // 3
	Bbonds.push_back(bd_eo) ;
//	Bbonds.push_back(bd_eo) ;
	Bbonds.push_back(bd_o) ; // 4
	int Blabel [] = {3, 100, 4} ;
	uni10::UniTensor B(Bbonds, Blabel, "B") ;
	B.randomize() ;
	B *= 10.0 ;

	vector<uni10::Bond> Cbonds ;
	Cbonds.push_back(bd_o) ;
	Cbonds.push_back(bd_eo) ;
	Cbonds.push_back(bd_eo) ;
	Cbonds.push_back(bd_o) ;
	int Clabel [] = {1, 2, 3, 4} ;
	uni10::UniTensor C(Cbonds, Clabel, "C") ;
	C.randomize() ;
	C *= 10.0 ;
	cout << C ;

//	uni10::UniTensor AB = contract(A, B, false) ;
//	AB.permute(AB.bondNum()) ;
//	cout << AB ;

//	uni10::UniTensor BA = contract(B, A, false) ;
//	BA.permute(BA.bondNum()) ;
//	cout << BA ;
//	BA.permute(AB.label(), AB.inBondNum()) ;
//	BA.permute(AB.label(), BA.bondNum()) ;
	uni10::UniTensor ABC = A * B * C ;
	cout << ABC ;

	ABC = B * C * A ;
	cout << ABC ;

	ABC = C * A * B ;
	cout << ABC ;

	ABC = B * A * C ;
	cout << ABC ;
}

void test_trace()
{
	uni10::Qnum qfe(uni10::PRTF_EVEN);
	uni10::Qnum qfo(uni10::PRTF_ODD);
	vector<uni10::Qnum> qnums ;
	qnums.push_back(qfe);
	qnums.push_back(qfo);
	uni10::Bond bd_eo(uni10::BD_IN, qnums);
	qnums.clear() ;
	qnums.push_back(qfe);
	uni10::Bond bd_e(uni10::BD_IN, qnums);
	qnums.clear() ;
	qnums.push_back(qfo);
	uni10::Bond bd_o(uni10::BD_IN, qnums);

	vector<uni10::Bond> Abonds ;
	Abonds.push_back(bd_eo) ;
	Abonds.push_back(bd_eo) ;
	Abonds.push_back(bd_eo) ;
	Abonds.push_back(bd_eo) ;
	Abonds.push_back(bd_e) ;
	int Alabel [] = {1, 2, 5, 6, 101} ;
	uni10::UniTensor A(Abonds, Alabel, "A") ;
	A.randomize() ;
	A *= 10.0 ;
//	cout << A ;

	vector<uni10::Bond> Bbonds ;
	Bbonds.push_back(bd_eo) ;
	Bbonds.push_back(bd_eo) ;
	Bbonds.push_back(bd_eo) ;
	Bbonds.push_back(bd_eo) ;
	Bbonds.push_back(bd_o) ;
	int Blabel [] = {2, 1, 7, 8, 102} ;
	uni10::UniTensor B(Bbonds, Blabel, "B") ;
	B.randomize() ;
	B *= 10.0 ;
//	cout << B ;

	vector<uni10::Bond> Cbonds ;
	Cbonds.push_back(bd_eo) ;
	Cbonds.push_back(bd_eo) ;
	Cbonds.push_back(bd_eo) ;
	Cbonds.push_back(bd_eo) ;
	Cbonds.push_back(bd_o) ;
	int Clabel [] = {3, 4, 6, 5, 103} ;
	uni10::UniTensor C(Cbonds, Clabel, "C") ;
	C.randomize() ;
	C *= 10.0 ;

	vector<uni10::Bond> Dbonds ;
	Dbonds.push_back(bd_eo) ;
	Dbonds.push_back(bd_eo) ;
	Dbonds.push_back(bd_eo) ;
	Dbonds.push_back(bd_eo) ;
	Dbonds.push_back(bd_e) ;
	int Dlabel [] = {4, 3, 8, 7, 104} ;
	uni10::UniTensor D(Dbonds, Dlabel, "D") ;
	D.randomize() ;
	D *= 10.0 ;

//	uni10::UniTensor AB = contract(A, B, false) ;
//	AB.permute(AB.bondNum()) ;
//	cout << AB ;

//	int Alabel_new [] = {1, 3} ;
//	A.permute(Alabel_new, A.bondNum()) ;
//	cout << A ;

//	int Blabel_new [] = {2, 1} ;
//	B.permute(Blabel_new, B.bondNum()) ;
//	cout << B ;

//	uni10::UniTensor BA = contract(B, A, false) ;
////	int BAlabel_new [] = {3, 2} ;
//	BA.permute(BA.bondNum()) ;
//	cout << BA ;
	uni10::UniTensor AB = A * B ;
	uni10::UniTensor CD = C * D ;
	uni10::UniTensor T = AB * CD ;
	T.permute(T.bondNum()) ;
	cout << T ;

	T = CD * AB ;
	T.permute(T.bondNum()) ;
	cout << T ;

	uni10::UniTensor DC = D * C ;
	uni10::UniTensor BA = B * A ;
	T = DC * BA ;
	T.permute(T.bondNum()) ;
	cout << T ;
	int Tlabel_new[] = {104, 102, 103, 101} ;
	T.permute(Tlabel_new, T.bondNum()) ;
	cout << T ;


}

int main()
{
//	test_fPEPS() ;
//	egU1() ;
	test_permute() ;
//	test_trace() ;

	return 0;
}




